import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PokedexModel } from '../models/pokedex-model';
import { Observable, pipe, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { NbToastrService } from '@nebular/theme';

@Injectable({
  providedIn: 'root'
})
export class PokedexResolver implements Resolve<PokedexModel> {

  constructor(
    private httpClient: HttpClient,
    private nbToastr: NbToastrService
  ) { }

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : PokedexModel | Observable<PokedexModel> | Promise<PokedexModel> {
      return this.httpClient.get<PokedexModel>("https://pokeapi.co/api/v2/pokemon")
        .pipe(catchError(e => {
          this.nbToastr.danger("La page n'a pas pu être chargée");
          return of(e);
        }));
  }
}
