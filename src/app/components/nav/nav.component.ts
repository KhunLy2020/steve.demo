import { Component, OnInit } from '@angular/core';
import { NbMenuItem } from '@nebular/theme';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {


  items: NbMenuItem[];

  constructor() { }

  ngOnInit(): void {
    this.items = [
      { link: '/home', title: 'Home', icon: 'home'},
      { link: '/about', title: 'About', icon: 'alert-triangle'},
      { title: 'Demo', icon: 'book', children: [
        { link: '/demo/demo1', title: 'Demo 1 - Binding'},
        { link: '/demo/demo2', title: 'Demo 2 - Events'},
        { link: '/demo/demo3', title: 'Demo 3 - Pokedex'},
        { link: '/demo/demo4', title: 'Demo 4 - @Input - @Output'},
      ]},
    ]
  }

}
