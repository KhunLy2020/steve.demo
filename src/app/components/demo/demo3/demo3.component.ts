import { Component, OnInit } from '@angular/core';
import { PokedexModel } from 'src/app/models/pokedex-model';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-demo3',
  templateUrl: './demo3.component.html',
  styleUrls: ['./demo3.component.scss']
})
export class Demo3Component implements OnInit {

  model: PokedexModel;

  temp: string;

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void {
    // this.httpClient.get<PokedexModel>("https://pokeapi.co/api/v2/pokemon")
    //   .subscribe(x => {
    //     this.model = x
    //   });
    this.model = this.route.snapshot.data.model;
  }

  onClick(url :string) {
    this.temp = url;
  }

}
