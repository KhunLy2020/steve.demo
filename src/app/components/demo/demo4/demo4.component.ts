import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators, ValidatorFn, FormControl } from '@angular/forms';

@Component({
  selector: 'app-demo4',
  templateUrl: './demo4.component.html',
  styleUrls: ['./demo4.component.scss']
})
export class Demo4Component implements OnInit {

  fg: FormGroup;

  constructor(
    private builder: FormBuilder
  ) { }

  ngOnInit(): void {
    this.fg = this.builder.group({
      date: [null, this.notBefore(new Date(2000,1,1))]
    })
  }

  onAnswer($event : boolean) {
    console.log($event);
    
  }

  notBefore(date: Date): ValidatorFn {
    return (control: FormControl) => {
      if(control.value == null) return null;
      if(control.value.getTime() < date.getTime()) {
        return { notBeforeError: "La date doit être inférieur à " + date }
      }
      return null;
    }
  }

}
