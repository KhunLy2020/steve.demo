import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';


@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {

  @Input() status;

  @Output() answer: EventEmitter<boolean>;

  constructor() {
    this.answer = new EventEmitter<boolean>();
  }

  ngOnInit(): void {
  }

  no() {
    this.answer.emit(false);
  }

  yes() {
    this.answer.emit(true);
  }

}
