import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo1',
  templateUrl: './demo1.component.html',
  styleUrls: ['./demo1.component.scss']
})
export class Demo1Component implements OnInit {

  maVariable: string = 'World';
  maVariable1: number = 1 / 7;
  maVariable2: Date = new Date();

  maVariable3: number = 243;

  constructor() { }

  ngOnInit(): void {
    setTimeout(() => {
      this.maVariable = 'Khun';
    },3000);
  }
}
