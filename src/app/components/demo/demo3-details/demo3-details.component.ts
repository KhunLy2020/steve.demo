import { Component, OnInit, Input } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-demo3-details',
  templateUrl: './demo3-details.component.html',
  styleUrls: ['./demo3-details.component.scss']
})
export class Demo3DetailsComponent implements OnInit {
  model: any;
  @Input() set url(v:string) {
    if(v == null) return;
    this.httpClient.get<any>(v).subscribe(x => this.model = x);
  }
  constructor(
    private httpClient: HttpClient
  ) { }

  ngOnInit(): void {
  }

}
